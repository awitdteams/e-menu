<!DOCTYPE html>
<?php
include("header.php");
?>
<!--header end-->
<!--sidebar start-->
<?php 
include("auth.php");
include("conn.php");
include("aside.php");
		//include("bookingtask.php");
$message="";
if (isset($_POST['start'])) {
			//date check//
	$start=$_POST['start'];
	$end=$_POST['end'];
	$start=date('Y-m-d',strtotime($start));
	$end=date('Y-m-d',strtotime($end));
	$sql="SELECT * FROM `booking` WHERE wedding_date>='$start' AND wedding_date<='$end' ";
}
//Confirm //
elseif(isset($_REQUEST['confirmid'])){
	$bookingid=$_REQUEST['confirmid'];
	$sql="UPDATE booking SET status= '1' WHERE booking_id='$bookingid' ";
/*$runquery=mysqli_query($conn,$sql);
if ($runquery) {
	echo "Update Successfully";
}

else{
	echo "No update";
}*/
if ($conn->query($sql)==TRUE) {
	$sql="SELECT * FROM `booking` WHERE booking_id='$bookingid' ";
	$message="<div class='alert alert-success' ><a href='#' class='close' data-dismiss='alert'></a> Success !</div>  ";
}else{
	$sql="SELECT * FROM `booking` WHERE status!=1";
}
}

//Delete
elseif (isset($_REQUEST['deleteid'])) {
	$bookingid=$_REQUEST['deleteid'];
	$sql="UPDATE `booking` SET status=3 WHERE booking_id='$bookingid' ";
	if ($conn->query($sql)===TRUE) {
		$sql="SELECT * FROM `booking` WHERE booking_id='$bookingid' ";
		$message="<div class='alert alert-success' ><a href='#' class='close' data-dismiss='alert'></a> Success !</div>  ";
	}else{
		$sql="SELECT * FROM `booking` WHERE status!=1";
	}
}	

?>
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<h2>Booking List</h2>
		<?php
					if(isset($_GET['updateid'])){
						include("updatebooking.php");
					}
					?>
		<div class="row">
		<!-- <div class="row">
			<form action="" method="POST" >
				<div class="col-sm-3">
					<div class="form-group">
						<label>From </label>
						<div  class="input-group">
							<input type="date" class="form-control" name="start"  required="" id="today"> 
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
</div>
<div class="form-group">
	<label>To </label>
	<div  class="input-group" data-d>
		<input type="date" class="form-control" name="end"  required=""> 
    <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
    </div>
</div>
</div>

<button class="btn btn-success">Search</button>


</div>
</form>
</div> -->
<br>

<div class="table-responsive">
	<table class="table table-hover table-bordered">
		<thead>
			<th>No.</th>
			<th>Name</th>
			<th>Package</th>
			<th>Wedding Date</th>
			<!-- <th>Email</th> -->
			<th>Phone</th>
			<th>Address</th>
			<th>Guest</th>
			<th>Booking Detail</th>
			<th>Booking Date</th>
			<th colspan="3" class="text-center">Action</th>
		</thead>
		<tbody>
			<?php
			$sql="SELECT * FROM booking  WHERE status!=3";
			$result=$conn->query($sql);
			if($result->num_rows>0){
				$i=1;
				while($row=$result->fetch_assoc()){
					$booking_id=$row['booking_id'];
					$user_id=$row['user_id'];
					$makeup_id=$row['makeup_id'];
					$mdress_id=$row['mandress_id'];
					$wdress_id=$row['womandress_id'];
					$food_id=$row['food_id'];
					$hall_id=$row['hall_id'];
					$photo_id=$row['photo_id'];
					$card_id=$row['card_id'];
					$gift_id=$row['gift_id'];
					$date=$row['wedding_date'];
					$time=$row['wedding_time'];
					$bdate=$row['booking_date'];
					$guest=$row['guestNo'];
					$cost=$row['totalCost'];
					$package=$row['package_id'];
					$status=$row['status'];
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php 
						$userid=$user_id;
						$sqls="SELECT * FROM user WHERE $userid=id";
						$results=$conn->query($sqls);
						if($results->num_rows>0){
							
							while($rows=$results->fetch_assoc()){
								$name=$rows['name'];
								$email=$rows['email'];
								$phone=$rows['phone'];
								$address=$rows['address'];
								echo "$name"; }
							}?></td>
							<td><?php 
							$packageid=$package;
							if($packageid!='0'){
						$package_id=$packageid;
						$sqls="SELECT * FROM package WHERE packageid='$package_id'";
						$results=$conn->query($sqls);
						if($results->num_rows>0){
							while($rows=$results->fetch_assoc()){
								$name=$rows['packagename'];
								echo "$name"; }
							}
						}else{
							echo "Customize";}?></td>
							<td><?php echo "$date/$time"; ?></td>
							<!-- <td><?php echo"$email";?></td> -->
							<td><?php echo "$phone";?></td>
							<td><?php echo "$address";?></td>
							<td><?php echo "$guest";?></td>
							<td><?php
							echo "<a class='btn xs-btn btn-info' href = 'bookingDetail.php?booking_id=".$row['booking_id']."'>View Details</a>" ?></td>
							<td><?php echo "$bdate" ?></td>
							<td ><?php if($status!=1){ echo"<a class='btn btn-success' href='index.php?confirmid=$booking_id' class='comfirm'>Confirm</a>";}?></td>
							<td id="update"><?php echo "<a class='btn btn-info update' href='index.php?updateid=$booking_id' >Update</a>" ?></td> 
							<td id="delete"><?php echo"<a class='btn btn-danger' href='index.php?deleteid=$booking_id' >Delete</a>" ?></td>
						</tr>

							<?php $i++;

						}
					}else{
						echo"<tr><td colspan='10'>Nothing Here!</td></tr>";
					}
					?> 
				</tbody>
			</table>
		</div>
	</div>
	</section>
	<?php
					/*if(isset($_GET['updateid'])){
						include("updatebooking.php");
					}*/
					?> 
					<script src="js/action.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
    $(".update").click(function(){
						id=$(this).data('booking_id');
						guest=$(this).data('guest');

						cost=$(this).data('cost');

						$("#bookingid").val(id);
						$("#guest").val(guest);
						$("#cost").val(cost);
						$("#myModal").modal('show');
					})
});
</script>
	
	<!-- footer -->

	<!-- //calendar -->


	
